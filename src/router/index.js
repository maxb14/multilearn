/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Practice from '@/components/Practice'
import Evaluation from '@/components/Evaluation'
import Account from '@/components/Account'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/practice',
      name: 'practice',
      component: Practice
    },
    {
      path: '/evaluation',
      name: 'evaluation',
      component: Evaluation
    },
    {
      path: '/account',
      name: 'account',
      component: Account
    }   
  ]
})
