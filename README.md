########### DM VUEJS ############
---------------------------------

Professeur : Stéphane BOUVRY
Réalisation : 
-> Paul LEVERRIER (21200453)
-> Maxime BLASZKA (21306584)
Promotion : M2 DNR2I
Année : 2017/2018

---------------------------------

Projet développé avec nodeJS

1) Ouvrir un terminal dans le dossier "multilearn"
2) Executer la commande suivante: "npm install"
3) Executer la commande suivante: "npm run dev"
4) Ouvrir un navigateur et entrer l'adresse suivante: http://localhost:8080

----------------------------------

Lien gitlab public: https://gitlab.com/maxb14/multilearn

